
ifeq ($(wildcard local.mk),)
  $(error You must first create and edit a local.mk file: cp example_local.mk local.mk; vi local.mk)
endif

# bring in your local settings
# you should copy example_local.mk to local.mk
# then edit all the entries, as necessary
include local.mk

defaults: request.txt $(SSH)/$(KEY)

local: authorized_keys $(SSH)/config_lshtm_hpc $(SSH)/config_repos

onhpc: $(SSH)/$(REPOKEY)

# constructs the content for requesting an LSHTM HPC acct
request.txt: local.mk
	@touch $@
	@echo "Subject: HPC Account Request" >> $@
	@echo "Name: $(YOURNAME)" >> $@
	@echo "Username: $(USERNAME) / $(LONGUSERNAME)" >> $@
	@echo "Department: $(DEP)" >> $@
	@echo "A brief description of what you plan to use the cluster for: $(WHY)" >> $@
	@echo "Software requirements: $(REQS)" >> $@
	more $@

# TODO: this a stub for automatically requesting an HPC account
sentmail: request.txt
	@echo "still in development; just paste request.txt into your normal email"
	@echo "sendmail -f $(EMAIL) -- servicedesk@lshtm.ac.uk < $^"
	$^ > $@

# this target creates ssh keys
$(SSH)/$(KEY) $(SSH)/$(REPOKEY): local.mk
	ssh-keygen -t rsa -b 4096 -C "$(EMAIL)" -f $@

# this expresses the relation between the public and private key,
# so that we can use the public key as a dependency later
%.pub: %

.PRECIOUS: %.pub

# WARNING: for these to work, your ssh config file (~/.ssh/config) needs to START with
# Include config_lshtm_hpc
# Include config_repos

$(SSH)/config_lshtm_hpc: $(SSH)/$(KEY)
	echo "Ensure your ~/.ssh/config file starts with 'Include $@'"
	@echo "Host $(ALIAS)" > $@
	@echo "  IdentityFile $^" >> $@
	@echo "  HostName $(HPC)" >> $@
	@echo "  User $(USERNAME)" >> $@
	more $@

$(SSH)/config_repos: $(SSH)/$(REPOKEY)
	echo "Ensure your ~/.ssh/config file starts with 'Include $@'"
	@echo "Host $(REPOALIAS)" > $@
	@echo "  IdentityFile $^" >> $@
	@echo "  HostName $(REPOS)" >> $@
	@echo "  User git" >> $@
	more $@

# WARNING: this will overwrite authorized_keys on the remote end
# WARNING: don't use this on hpc
# TODO: rsync authorized_keys to here, then append, then rsync back?
authorized_keys: $(SSH)/$(KEY).pub
	scp $(USERNAME)@$(HPC):~/.ssh/$@ .
	cat $^ >> $@
	scp $@ $(USERNAME)@$(HPC):~/.ssh/

# for making SGE qsub templates
# these most of these variables can be set at CL when invoking make
# though that may be more tedious than just editing the resulting script files
%.sge: array.sge.tmp local.mk
	@sed 's/EMAIL/$(EMAIL)/' $< | \
	sed 's&HPCPRJROOT&$(HPCPRJROOT)&' | \
	sed 's&HPCOUT&$(HPCOUT)&' | \
	sed -e 's/HH/$(HH)/' -e 's/MM/$(MM)/' -e 's/SS/$(SS)/' | \
	sed -e 's/RAM/$(RAM)/' -e 's/CORES/$(CORES)/' | \
	sed -e 's/LL/$(LL)/' -e 's/UL/$(UL)/' -e 's/STP/$(STP)/' | \
	sed -e 's/JOBNAME/$(JOBNAME)/' > $@
	more $@


rsetup: $(HPCRLIB)

$(HPCRPKGLIST): examplerlibs.txt
	cp $^ $@

# only need to use this on the hpc
$(HPCRLIB): install.R $(HPCRPKGLIST) | ~/.Rprofile
	mkdir -p $@
	Rscript $^

~/.Rprofile: local.mk
	@echo ".libPaths(c('$(HPCRLIB)',.libPaths()))" > $@
	@echo 'local({r <- getOption("repos"); r["CRAN"] <- "https://cloud.r-project.org"; options(repos=r)})' >> $@
	more $@
