# only need to change if adopting to some other system
DOMAIN := lshtm.ac.uk
HPC := hpclogin.$(DOMAN)
REPOS := gitlab.com

# edit this for your details
YOURNAME := John Smith
USERNAME := eidejsmi
LONGUSERNAME := john.smith
DEP := IDE
WHY := Need HPC access for modelling and simulation work, as part of project XYZ.
REQS := R, C++14 (or greater), make >4.0, git >2.0

HPCRLIB := ~/libs/Rpackages
HPCRPKGLIST := ~/libs/rlibs.txt

# generated for convenience in Makefile; shouldn't need to change directly
EMAIL := $(LONGUSERNAME)@$(DOMAIN)
SSH := ~/.ssh
KEY := hpc
ALIAS := lshtm-hpc
REPOKEY := gitrepos
REPOALIAS := repos

# definitions for useful standard names; used in various templates
# these are `?=` to allow overriding them by invoking `make sometemplate.sge SOMEVAR=X`

# shouldn't need to change this, unless you have strange HPC username
HPCHOME := /home/$(USERNAME)
# default source directory
HPCPRJROOT ?= $(HPCHOME)/workspaces/lshtm-hpc-utils
# default output directory
HPCOUT ?= $(HPCHOME)/output/lshtm-hpc-utils

# default job wallclock hours
HH ?= 00
# default job wallclock minutes
MM ?= 05
# default job wallclock seconds
SS ?= 00
# default RAM (gigs; must be integer)
RAM ?= 1
# default cores for multi-core jobs
CORES ?= 2
# default job name
JOBNAME ?= CHANGEME
# default lower limit for task ids; WARNING: must be positive integer (i.e. 1 or more)
LL ?= 1
# default upper limit for task ids; WARNING: must be positive integer > LL
UL ?= 6
# default step size for array jobs
STP ?= 1
