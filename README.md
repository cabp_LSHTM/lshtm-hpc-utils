# lshtm-hpc-utils

Documentation on using the LSHTM HPC + convenience tools.

# HIC FORSIT SVNT DRACONES

This repository has a `Makefile` with several targets that *might* automate this process for you. If they don't (because I have failed to make them sufficiently flexible, likely), the steps are also explained so that you can do them by hand.

The steps are roughly:

 - have a command line
 - use a repository service & git on your local machine(s)
 - clone this repo to your local machine
 - configure for you
 - get access to hpc
 - with access to hpc, clone this repo there
 - setup access to your repository service from the hpc
 - get to work

I have listed this first three steps explicitly, but without instructions below, as the minimum expectation for using this guide.  This guide is no doubt imperfect, but I will not entertain questions / feedback from people that cannot meet that standard first.  Window users will need something other than `cmd` for command line; I understand `powershell` and `gitbash` work pretty well.  If someone wants to write a Windows-specific portion, I will happily include it and re-direct questions to you.

## Configure for You

The automatic components rely on setting some variables in a `local.mk` file, which is imported by the main `Makefile`. I would appreciate someone writing a command line script that will create such a file, but that doesn't yet exist. For now, the steps are:

```
$ cp example_local.mk local.mk
$ vi local.mk
```

or any other plaintext editor vice `vi`.  When you edit `local.mk`, the must-edit section is the second block starting with `# edit this ...` You may also want to edit the repository linking content (`REPOS := ...`, `REPOKEY := ...` settings). I prefer gitlab and have written the defaults that way, but (statistically) you prefer github.com. Or maybe you're fussier than I, and use bitbucket.  Whatever floats your boat.

## Get access to the LSHTM HPC

Summary: `make sentmail` and then follow the on-screen instructions.

This related to registering with the school IT services to get access.  You should be able just copy-paste that material into an email to servicedesk@lshtm.ac.uk (send from lshtm.ac.uk account).  But if it doesn't work, you need to email them with the following information:
 - Name: First Last
 - Username: (both the encoded one & first.last for normal email)
 - Department (e.g., IDE)
 - A brief description of what you plan to use the cluster for.
 - Software requirements (e.g., R, C++14, git, particular libraries, etc)

## Improve Access to LSHTM HPC

Summary: `make local` then follow the directions about editing `~/.ssh/config`

Once you have basic access, you should configure your machine(s) to have `ssh` interactions with the HPC. To do that, you'll need an ssh key, and will need to tell the HPC about that key.  This has a few parts:
 - creating the key
 - telling your local machine to use the key to talk to the hpc
 - telling the hpc to accept the key

If the automated part doesn't work, use `ssh-keygen` to create the key:

`cd ~/.ssh; ssh-keygen -t rsa -b 4096 -C "YOUREMAIL" -f AKEYNAME`

Then tell the hpc about your new key:

`cd ~/.ssh; cp AKEYNAME.pub authorized_keys; scp authorized_keys YOURUSENAME@hpclogin.lshtm.ac.uk:~/.ssh/; rm authorized_keys`

Then tell your machine to use this key when talking to the hpc.  When newer versions of ssh, you can define separate config files then include them in the main `~/.ssh/config` file.  If you have to use old version of ssh for some reason, just put content in there.

```
cd ~/.ssh
echo "Ensure your ~/.ssh/config file starts with 'Include ~/.ssh/config_lshtm_hpc'"
echo "Host lshtm-hpc" > ~/.ssh/config_lshtm_hpc
echo "  IdentityFile ~/.ssh/AKEYNAME" >> ~/.ssh/config_lshtm_hpc
echo "  HostName hpclogin.lshtm.ac.uk" >> ~/.ssh/config_lshtm_hpc
echo "  User YOURUSENAME" >> ~/.ssh/config_lshtm_hpc
more ~/.ssh/config_lshtm_hpc
```

Then make sure your `~/.ssh/config` starts with `Include ~/.ssh/config_lshtm_hpc` (can come before or after other Include directives, but should be ahead of any Host definitions).

## Improve Access to Your Repository Service

Summary: `make local` on your local machine(s) then on cluster `make onhpc`

This is setting up the same ssh key based relations with your repository service and your machine(s) and the hpc.  While you might use a GUI on your personal machine, that won't be as effective on the hpc and many hpc setups (unsure about LSHTM's) do not support `https` protocol.

If the automatic approach doesn't work, repeat the steps from the previous section (except the `authorized_keys` step) to make another key on your local machine (for interacting with repository service(s)) and one on the hpc.  You will then need to follow the service-specific instructions about registering the public fingerprints of these two keys.

You can use the same key with multiple services by duplicating the entry in the `~/.ssh/config_repos` and changing the alias and hostname. If someone wants to tweak the `make` rules to support multiple services, that would be a fun improvement / learning opportunity.

# Getting To Work

There are a few other basic utilities available via `make`:
 - setting up a local R library
 - using templates to make scripts

## R library

Summary: `make rsetup`, though you probably want to copy-edit your desired libraries first.

## Templates

TODO!
